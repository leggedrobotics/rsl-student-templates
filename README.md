Student Templates
=====================

This repository is here to orient the students who conduct a project (Studies on Mechatronics, Bachelor-, Semester-, and Master Theses) at the Robotic Systems Lab (RSL) at ETH Zurich. It contains general information, guidelines, templates and helpful links.

Head over to the [Wiki](https://bitbucket.org/leggedrobotics/rsl-student-templates/wiki/Home) for more information or download the entire repository as a ZIP-file [here](https://bitbucket.org/leggedrobotics/rsl-student-templates/get/master.zip).

RSL student projects are listed here: [http://www.rsl.ethz.ch/education-students/student-projects0.html](http://www.rsl.ethz.ch/education-students/student-projects0.html).
